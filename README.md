# Chatbot Real Time Socket.Io and OpenAI - Node - Express - MongoDB

# Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

# Prerequisites

You'll need to have Node.js and npm (Node Package Manager) installed on your machine. You can download and install them from the official website if you don't already have them.

# Installing

Clone the repository to your local machine

```
git clone https://gitlab.com/abhimishra271098/chatbotopenai.git
```

Navigate to the project directory

```
cd nodejsAIAssistant
```

Install all dependencies

```
npm install
```

Create a .env file in the project root directory and add your OpenAI API key


Run app

```
npm start
```

## Built With

* Node.Js
* Express.Js
* MongoDB
* Socket.IO
* openai
* dotenv
* nodemon
* ejs


## Author

**abhishek**

