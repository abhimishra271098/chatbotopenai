// Connect to the socket.io server using the current location
const socket = io.connect(location.href);

// DOM elements
const message = document.querySelector("#message");
const handle = document.querySelector("#user");
const btn = document.querySelector("#send");
const output = document.querySelector("#output");
const feedback = document.querySelector("#feedback");

// Event listener for the "Send" button
btn.addEventListener("click", () => {
  if (message.value !== "") {
    // Create a data object with user's message and handle
    const data = {
      message: message.value,
      handle: "User",
    };
    socket.emit("chat", data);
    appendMessage(data, "sender-message"); // Display the sender's message on the right
  } else {
    alert("All fields are required!");
  }
  message.value = "";
});

// Event listener for typing
message.addEventListener("keypress", () => {
  socket.emit("typing", handle.value);
});

// Event listener for incoming chat messages
socket.on("chat", (data) => {
  appendMessage(data, "receiver-message"); // Display the receiver's message on the left
});

let timer = setTimeout(makeNoTypingState, 1000);
socket.on("typing", (data) => {
  feedback.innerHTML = `<p><em>${data} is typing a message...</em></p>`;
  clearTimeout(timer);
  timer = setTimeout(makeNoTypingState, 1000);
});

// Function to append a message to the chat output
function appendMessage(data, className) {
  const messageDiv = document.createElement("div");
  messageDiv.className = className;
  messageDiv.innerHTML = `<p><strong>${data.handle}: </strong>${data.message}</p>`;
  output.appendChild(messageDiv);
}

// Function to clear the typing feedback
function makeNoTypingState() {
  feedback.innerHTML = "";
}
