// Import the Mongoose library
const mongoose = require("mongoose");

// Define the schema for the 'chat' collection
const chatSchema = new mongoose.Schema({
  name: String,
  message: String,
});

// Create and export the 'chat' model
module.exports = mongoose.model("Chat", chatSchema);
