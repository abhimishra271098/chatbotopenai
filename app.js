// Import required libraries and modules
const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const OpenAI = require("openai");
const dotenv = require("dotenv");
const Chat = require("./models/Chat");

// Load environment variables from a .env file
dotenv.config();

// Connect to the database
require("./config/db-connection");

// Serve static files from the "public" directory
app.use("/public", express.static("public"));

// Set the view engine to EJS for rendering templates
app.set("view engine", "ejs");

// Initialize the OpenAI API client with your API key
const openai = new OpenAI({
  apiKey: process.env.AIKEY,
});

// Define a route handler for the root ("/") path
app.get("/", (req, res) => {
  // Find all Chat messages in the database
  Chat.find({})
    .then((messages) => {
      // Render the "index" template and pass the retrieved messages
      res.render("index", { messages });
    })
    .catch((err) => {
      // Handle any errors and log them
      console.error(err);
    });
});

// Set up a WebSocket connection for handling chat and typing events
io.on("connection", (socket) => {
  // Handle incoming chat messages
  socket.on("chat", (data) => {
    // Send the user's message to openai and get a response
    openai.completions
      .create({
        model: "text-davinci-002", // Specify the openai model
        prompt: data.message,
        max_tokens: 500, // Adjust the token limit as needed
      })
      .then((response) => {
        const aiMessage = response.choices[0].text;

        // Create chat messages for both user and AI
        Chat.create({ name: "User", message: data.message });
        Chat.create({ name: "AI", message: aiMessage });

        // Broadcast the AI's response to all connected clients
        io.sockets.emit("chat", { handle: "AI", message: aiMessage });
      })
      .catch((err) => {
        // Handle any errors and log them
        console.error(err);
      });
  });

  // Handle typing notifications
  socket.on("typing", (data) => {
    // Broadcast typing notifications to all other clients
    socket.broadcast.emit("typing", data);
  });
});

// Start the HTTP server and listen on the specified port
http.listen(process.env.PORT, () => {
  console.log("Server is running on port " + process.env.PORT);
});
