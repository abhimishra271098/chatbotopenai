// Load environment variables from a .env file
require("dotenv").config();

// Import the Mongoose library
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

// MongoDB setup
mongoose.connect(process.env.DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection
  .once("open", () => {
    console.log("Connected to the database");
  })
  .on("error", (err) => {
    console.error(err);
  });
